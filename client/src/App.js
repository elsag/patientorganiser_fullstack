import './App.css';
import React from 'react';
import { EasybaseProvider } from 'easybase-react';
import ebconfig from './ebconfig';
import Corps from './Corps.js';



function App() {
  return (
    <div className="App" >

      <EasybaseProvider ebconfig={ebconfig}>
        
        <Corps />
        
      </EasybaseProvider>
    </div>
  );
}

export default App;

