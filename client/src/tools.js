
export function getAllIndexes(arr, val) {
    var indexes = [], i = -1;
	
	for (let i = 0; i < arr.length; i++){
		if (arr[i] === val) {
			indexes.push(i); 
		}
	}
    /*while ((i = arr.indexOf(val, i+1)) != -1){
        indexes.push(i);
    }*/
    return indexes;
}

export function getIndexesItems(arr, iDB, indexes) {
	var items =[];
	for (let i = 0; i < indexes.length; i++){
		if(indexes.includes(iDB[i]))
		{ items.push(arr[i]); }
	}
	return items;
}
export function getDatesNotes(Frame, indexes){
	var dates=[];
	var notes=[];
	var id=Frame.map(x=>x.id);
	var alldates=Frame.map(x=>x.Date);
	var allnotes=Frame.map(x=>x.Note);	
	/* console.log("alldates", alldates);
	console.log("allnotes",allnotes);
	console.log("id", id);
	console.log("indexes",indexes); */
	for ( let i=0; i<id.length; i++ ){
		if (indexes.includes(id[i])){
			dates.push(alldates[i]);
			notes.push(allnotes[i]);
		}
	
	}
	/* console.log("dates",dates);
	console.log("notes",notes); */

	return {dates, notes};
}

export function getIndexesFromDB(iDB, iName){
	var id =[];
	for( let i=0; i<iName.length; i++){
		id.push(iDB[iName[i]]);
	}
	return id;
}

export function keepUniqueItems(arr){
	var sort_arr= arr.sort(), unique_arr=[], last= '';

	for (let i = 0; i < sort_arr.length; i++){
		if (sort_arr[i] != last) {
			unique_arr.push(sort_arr[i]); 
			last=sort_arr[i];}
	}
	return unique_arr;

}


export default {getAllIndexes, getIndexesItems, keepUniqueItems, getIndexesFromDB,getDatesNotes}