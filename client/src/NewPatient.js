import React from "react";
import './index.css';
import { useEasybase } from "easybase-react";



export default function NewPatientButton({handleClick}) {

  /*Load database*/
  const { Frame, sync } = useEasybase();

  /*var of the new patient's information*/
  var NewPatientName='', NewPatientBirthday='', NewPatientNum='', NewPatientMail='', newPatientNote='';
  
  /*Save new patient's information and display information of the patient*/  
  const handleSave = () => {

    Frame().push({
        name: NewPatientName,
        note: newPatientNote,
        birthday: NewPatientBirthday,
        mail: NewPatientMail,
        phone: NewPatientNum,
        date: new Date().toISOString()
      })

    sync();  

    handleClick(NewPatientName);

  }

  /*var PatientsNames = keepUniqueItems(Frame().map(x => x.name))*/

  /*Change var on user changes*/
  const updateName=(value) =>{
    NewPatientName = value;
  }

  const updateBirthday=(value) =>{
    NewPatientBirthday = value;
  }

  const updateNum=(value) =>{
    NewPatientNum = value;
  }

  const updateMail=(value) =>{
    NewPatientMail = value;
  }

  const updateNote=(value) =>{
      newPatientNote = value;
  }

  return (

    <div>

          <div className="NewPatientArea"  >
            
            <div className="headerConsultationStyle"> <h3 style={{color:"#ffffff"}}> Nouveau Patient </h3></div>

            <div className="content">
                
                <div className="entry">
                  <p style={{paddingRight:10}}>Nom du patient : </p>
                  <input className="InputNewPatient" type="string" onChange={e => updateName(e.target.value)}/><br />
                </div>

                <div className="entry">
                  <p style={{paddingRight:10}}>Année de naissance : </p>
                  <input className="InputNewPatient" type="string" onChange={e => updateBirthday(e.target.value)}/><br />
                </div>

                <div className="entry">
                  <p style={{paddingRight:10}}>Téléphone : </p>
                  <input className="InputNewPatient" type="string" onChange={e => updateNum(e.target.value)}/><br />
                </div>

                <div className="entry">
                  <p style={{paddingRight:10}}>Email : </p>
                  <input className="InputNewPatient" type="string" onChange={e => updateMail(e.target.value)}/><br />
                </div>
                <div className="entry">
                  <p style={{paddingRight:10}}>Notes : </p>
                  <textarea className="InputNewNotes" type="string" onChange={e => updateNote(e.target.value)}> </textarea>
                </div>

          			<br />

          			<div className="alignBottomRight">
          				<button className="Button" onClick={() => handleSave()}><h3 style={{color:"#ffffff"}}>Enregistrer</h3></button>
          			</div>
              
            </div>
            
          </div> 
                
      </div>

  )
  
}
