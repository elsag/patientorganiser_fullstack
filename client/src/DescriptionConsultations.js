import React, {useState} from "react";
import './index.css';
import { useEasybase } from "easybase-react";
import {getDatesNotes,getIndexesItems} from './tools.js'
import TextNote from './TextNote.js';
import ModifyNote from './ModifyNote.js';
import Axios from "axios";


export default function DescriptionConsultations({handleConsultation,Frame, indexes, handleAccueil}) {


	/*Find date and note of the corresponding patient in the database*/
	/*var DataFrame= getIndexesItems(Frame.map(x => x.Date), Frame.map(x => x.id), indexes) ;*/
	var data = getDatesNotes(Frame, indexes) ;
	var DataFrame= data.dates;
	var DescriptionFrame= data.notes;

	/* console.log("Dataframe", DataFrame);
	console.log("DescriptionFrame", DescriptionFrame); */

	/*var DescriptionFrame = getIndexesItems(Frame.map(x => x.Note), Frame.map(x => x.id),indexes);*/

	/*set state of the displayed area*/
	var [active, setActive]= useState("Text");

	var [inc, setInc]=useState(0);

	/*Call parent fonction*/
	const handleToNew = () => {
		handleConsultation();
  	}

  	/*function call to change description depending of the mode: Text*/
  	const setState = (i) => {
		setInc(i);
		setActive("Text");
  	}

  	/*Modify note in the database*/
  	function handleModify(Notes, id){
  		const idFrame=indexes[id];
		  

		Axios.patch("http://localhost:3001/api/patch", {

			Id:idFrame,
			Note:Notes,

			}).then(()=>{
				alert("sucessful insert");
			});/*.catch((err)=>{
				alert("erreur when changing data new data");
			});*/


  		setActive("Text");
  		setInc(id);

  	}

  	/*function call to change description depending of the mode: Modify*/
	function handleNote(id){
		
  		setActive("Modify");
  		setInc(id);
  	}

  	/*Delete entry in the database*/
  	function handleDelete(id){
  		const idFrame=indexes[id];
		console.log("idTo Deleste", idFrame);
		/*------------------Modify to udpate databse */
		Axios.delete('http://localhost:3001/api/delete', {Id:idFrame}).then(()=>{    
			alert("sucessful delete");
			console.log("SUCCESSSSSS");
		  }).catch((err)=>{
			alert(err);
		});

		console.log("idTo Deleste AXIOS fini");
		indexes.splice(id);

		/* DataFrame = getIndexesItems(Frame.map(x => x.Date), indexes) ;
		DescriptionFrame = getIndexesItems(Frame.map(x => x.Note), indexes);

		console.log(DescriptionFrame)
		console.log("indexes:",indexes) */
		
		
  		setActive("Text");
  		setInc(0);
  		
  	}

	return(

		<div className="ConsultationArea" >

			<div className="leftColumn">

				<button className="ButtonNew" onClick={() => handleToNew()}><h3 >Nouvelle note</h3></button>

				{DataFrame.map((x,i) =>  

			      	<button className="PatientDate" onClick={()=>{setState(i)}}>
			      		<p className="DateStyle">{String(x.slice(8,10))}.{String(x.slice(5,7))}.{String(x.slice(0,4))}</p>
			      	</button>

		      	)}

			 </div>

			 <div className="rightColumn">
	  
				<div className="DescriptionArea" >

					{active === "Modify" && <ModifyNote handleModify={handleModify} Description={DescriptionFrame[inc]} id={inc} />}
					{active === "Text" && <TextNote handleNote={handleNote} handleDelete={handleDelete} Description={DescriptionFrame[inc]} id={inc} />}

		     	</div>
	  		</div>
					
		</div>
		
	)

}
