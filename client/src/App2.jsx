import React, {useState, useEffect}  from 'react';
//import { useEasybase } from "easybase-react";
import Patient from './Patient.js';
import './index.css';
import {keepUniqueItems} from './tools.js'
import NewPatientButton from './NewPatient.js';
import logo from './logo_cuure.png';
import Accueil from './Accueil.js'
import Axios from "axios";


function App() {
    const [Frame, setFrameList] = useState([]);
   
    /*state of the timer */
  	/*const [seconds, setSeconds]= useState(0);*/

    /*Load database*/
    //const { Frame, sync, configureFrame } =useEasybase();
      
    /*set State for area displayed*/
    const [active, setActive]= useState("Accueil");
  
    const [name, setName]=useState("Name");
  
    /*allow to reset state of child when selecting a new patient*/
    const [resetState, setResetState]=useState(0);
    
    /*Hook useEffect with interval to reload each seconds the data */
   /*  useEffect(()=>{
      const interval = setInterval(()=>{
      Axios.get("http://localhost:3001/api/get").then((response)=>{
     
        setFrameList(response.data);

      })}, 1000);

      return() => clearInterval(interval);
      
    },[]); */

    useEffect(()=>{
      
      Axios.get("http://localhost:3001/api/get").then((response)=>{    
        setFrameList(response.data);
      });      
    },[]);


    /*Take all patient name only once*/
    var PatientsNames = keepUniqueItems(Frame.map(x => x.Name)).sort();
  
  
    /*Function call when press on a patient's name*/
    function handleClick(ele){
            setName(ele);
            setActive("Patient");
            setResetState(resetState + 1);
        //submitData();
  
        }
  
    function handleAccueil(){
      setActive("Accueil");
    }
  
    return (
  
      <>
        <div className="headerApp">
  
            <button className="NewPatientButton" onClick={()=>setActive("New")}>
              <h3>Nouveau Patient</h3>
            </button>
  
            <img className="logo" src={logo} alt="Logo" />
            
        </div>
  
        <div className="PatientsList" >
                  
            {PatientsNames.map((ele,i) => 
                  <button className="namesButton" onClick={()=>{handleClick(ele)}} key={i}>
                      <p>{ele}</p>
                  </button>        
            )}
  

        </div>
        
            <div>
                {active === "Accueil" && <Accueil />}
                {active === "Patient" && <Patient name={name} key={resetState} handleAccueil={handleAccueil} Frame={Frame} />}
                {active === "New" && <NewPatientButton handleClick={handleClick} />}
            </div>
  
  
          
  
        
      </>
    )
  
}

export default App;



