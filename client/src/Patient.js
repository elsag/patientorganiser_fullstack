import React, {useState} from "react";
import './index.css';
import NewConsultation from './NewConsultation.js';
import DescriptionConsultations from './DescriptionConsultations.js';
import {getAllIndexes, getIndexesFromDB} from './tools.js';
import Axios from "axios";


export default function Patient({name, handleAccueil,Frame}){


	/*state of the display area*/
	const [active, setActive]= useState("Description");

	

	/*Find all existing patient name*/
	const name_list = Frame.map(x => x.Name);

	/*Find all indexes in the database with the name of the selected patient*/
	var indexesName = getAllIndexes(name_list, name);

	var indexesDB =Frame.map(x => x.id);

	var indexes= getIndexesFromDB(indexesDB,indexesName);
	/* console.log("indexes :", indexes);
	console.log("frame15", Frame); */


	
	/*Callback function to change display*/
	function handleConsultation(){
		setActive("New");
		

    }

    function handleReturn(){
		setActive("Description");
		
    }

    //function to pass data to the backend 
    function handleNew(notes){
		  
		Axios.post("http://localhost:3001/api/insert", {
	
			Name:name,
			Birthday:Frame[indexesName[0]].Birthday,
			Phone:Frame[indexesName[0]].Phone,
			Mail:Frame[indexesName[0]].Mail,
			Note:notes,
			Date:new Date().toISOString(),
	
		}).then(()=>{
			alert("sucessful insert");
		}).catch((err)=>{
			alert(err);
		});
		
	    setActive("Description");
      
    }

	return (

		<div className="PatientArea">
		        
	        <div className="headerPatientDate" > 

		        <h3 className="nameUnderline"> {name} </h3> 

		        <div className="InfoPatient">

   					<div className="displayInfo"  ><p className="groupInfo">Téléphone </p> <p className="patientInfo"> {Frame[indexesName[0]].Phone}</p> </div>
			        <div className="displayInfo" ><p className="groupInfo"> Email </p> <p className="patientInfo"> {Frame[indexesName[0]].Mail}</p></div>
			        <div className="displayInfo" ><p className="groupInfo"> Age </p> <p className="patientInfo"> {Frame[indexesName[0]].Birthday}</p> </div>
			     
	        	</div>

	        </div>

	        <div className="content">
	         
				{active === "Description" && <DescriptionConsultations handleConsultation={handleConsultation} handleAccueil={handleAccueil} Frame={Frame} indexes={indexes} />}
				{active === "New" && <NewConsultation handleNew={handleNew} handleReturn={handleReturn} />}

	         </div>
		       
	   	</div>
	)
}
