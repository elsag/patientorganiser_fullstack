import React, {useState, useEffect} from "react";
import { useEasybase } from "easybase-react";
import Patient from './Patient.js';
import './index.css';
import {keepUniqueItems} from './tools.js'
import NewPatientButton from './NewPatient.js';
import logo from './logo_cuure.png';
import Accueil from './Accueil.js'
//axios to send info to backend code
import Axios from "axios";


export default function Corps(){
  const [movieList, setMovieList] = useState([]);

  useEffect(()=>{
    Axios.get("http://localhost:3001/api/get").then((response)=>{
      setMovieList(response.data);
    });
  }, []);

  //function to pass data to the backend 
  const submitData = () =>{
    const movieName='nomovie';
    const movieReview='not good';
    Axios.post("http://localhost:3001/api/insert", {

      movieName:movieName,
      movieReview:movieReview,
    }).then(()=>{
      alert("sucessful insert");
    });
  };
	
  /*Load database*/
	const { Frame, sync, configureFrame } =useEasybase();
	
  /*set State for area displayed*/
	const [active, setActive]= useState("Accueil");

	const [name, setName]=useState("Name");

  /*allow to reset state of child when selecting a new patient*/
  const [resetState, setResetState]=useState(0);

	/*Take back the NOTES CONSULTATION table available on easybase*/
	useEffect(() => {
	    configureFrame({ tableName: "NOTES CONSULTATION", limit: 10 });
	    sync();
	  }, []);

  /*Take all patient name only once*/
	var PatientsNames = keepUniqueItems(Frame().map(x => x.name)).sort();


  /*Function call when press on a patient's name*/
  function handleClick(ele){
  		setActive("Patient");
  		setName(ele);
      setResetState(resetState + 1);
      submitData();

  	}

  function handleAccueil(){
    setActive("Accueil");
  }

  return (

    <>
      <div className="headerApp">

          <button className="NewPatientButton" onClick={()=>setActive("New")}>
            <h3>Nouveau Patient</h3>
          </button>

          <img className="logo" src={logo} alt="Logo" />
          
      </div>

      <div className="PatientsList" >
                
          {PatientsNames.map(ele => 
             
          	
            	
            	<button className="namesButton" onClick={()=>{handleClick(ele)}}>
            		<p>{ele}</p>
            	</button>
        
          )}

          <div>
          {
            movieList.map((val)=>{
              return <h1> MovieName: {val.movieName}, MovieREview: {val.movieReview}</h1>

            })
          }
        </div>
      </div>
      
        	<div>
        		{active === "Accueil" && <Accueil />}
        		{active === "Patient" && <Patient name={name} key={resetState} handleAccueil={handleAccueil} />}
        		{active === "New" && <NewPatientButton handleClick={handleClick} />}
        	</div>


        

      
    </>
  )


}

