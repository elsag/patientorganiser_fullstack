## How to install and run the web-app

It is necessary to have Node.js installed. It is possible to install it from the following url: https://nodejs.org/en/

Create a new file and from the bash shell, go inside the created file and run:
	git clone https://gitlab.com/elsag/patientorganizer.git

Go inside the patientorganizer file:
	cd patientorganizer

Then run the following command (It is possible that it takes several minutes to run):
	npm install 

And finally:
	npm run start

If everything goes fine, a new page should open in your browser with the web-app.

