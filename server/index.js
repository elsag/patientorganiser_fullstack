// construct express server
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const mysql = require('mysql2');

// needed to make a change in terminal: npm un mysql && npm i mysql2
	
const db = mysql.createPool({
	host:'127.0.0.1',
	user:'root',
	port:'3306',
	password:'root299$',
	database:'newDB',
})

app.use(cors());

// the following line is necessary if you request something from the front end
app.use(express.json())

app.use(bodyParser.urlencoded({extended:true}));

app.get("/api/get", (req, res)=>{
	const sqlSelect = "SELECT * FROM patientOrganizer";
	db.query(sqlSelect, (err,result)=>{
		res.send(result);
	});
})

app.post("/api/insert", (req,res)=> {
	const Name = req.body.Name;
	const Note = req.body.Note;
	const Birthday = req.body.Birthday;
	const Mail = req.body.Mail;
	const Phone = req.body.Phone;
	const Date = req.body.Date;

	const sqlInsert = "INSERT INTO patientOrganizer (Name, Note,Birthday,Mail,Phone,Date) VALUES (?,?,?,?,?,?)";
	db.query(sqlInsert, [Name, Note, Birthday, Mail, Phone, Date], (err,result)=>{
		if(err){console.log("erreur");};
		console.log(result);
	});
});

app.delete("/api/delete",(req)=>{
	const Id =req.body.Id;

	/* const {id}=req.params.id; */
	const sqlDelete = `DELETE FROM patientOrganizer WHERE id =${Id}`;
	db.query(sqlDelete,(err)=>{
		if(err){console.log(err);};
		
	});
})

app.patch("/api/patch",(req,res)=>{
	const Note = req.body.Note;
	const Id = req.body.Id;
	const sqlChange = "UPDATE patientOrganizer SET Note = ? WHERE id = ?";
	//"UPDATE patientOrganizer SET Note = ? WHERE id = ?"
	db.query(sqlChange,[Note, Id], (err,result)=>{
		if(err){console.log(err);};
	});
})

app.listen(3001, ()=>{
	console.log("running on port 3001");
});

/*script.get('/updatepost/:id', (req, res) => {
    let sql = `UPDATE posts SET name = '${req.body.name}', school='${req.body.school}' WHERE id = ${req.params.id}`;
    let query = connection.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Post updated...');
    })
});
let sql = UPDATE posts SET name = '${newName}', school='${newSchool}' WHERE id = ${req.params.id};*/
